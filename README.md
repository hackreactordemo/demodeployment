# Deployement on AWS EC2!

## Getting started

This is a repository featuring a small application. We will deploy this app to AWS and serve the frontend with Gitlab.

To start this project locally run `docker compose up --build` from the root directory.

The accompanying Instructions for deploying with AWS and Gitlab are featured in our Learn curriculum. Happy Coding! 
